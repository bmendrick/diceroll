#DiceRoll project

This project is a string parser for rolling dice in Python2.

In particular, this project uses Random.org's rdoclient in order to generate the random number required for dice rolls.

#IMPORTANT
You will need to place your Random.org API key into the apikey.txt file.
API keys can be generated here: https://api.random.org/api-keys

This repo is available under GNU GPL.

# -*- coding: utf-8 -*-
"""DiceRoll

This file contains code to parse basic input strings in order to
randomly roll a set of dice. Randomness comes from the Random.org
rdoclient.

For usage information run:
	python diceroll.py -h
		OR
	python dicroll.py --help

Author: Brandon Mendrick

Code available under the GNU GPL.

"""

from argparse import ArgumentParser
from argparse import RawTextHelpFormatter
from rdoclient import RandomOrgClient
from re import match
from sys import exit

class DiceError(Exception):
	"""Exception class for DiceRoll errors."""
	def __init__(self, msg):
		self.msg = msg

	def __str__(self):
		return self.msg

class DiceRoll:
	"""DiceRoll

	This class is spawned once in the main program and controls all
	interactions with the rdoclient.

	"""

	def __init__(self, args, input_, user_input):
		"""Intialization of DiceRoll class.

		Sets the passed in parameters. Retrieves the Random.org api key
		from the "apikey.txt" file. Setup rdoclient.

		"""
		self.args = args
		self.input = input_
		self.user_input = user_input

		# get the Random.org API key
		self.apikey = None
		with open('apikey.txt', 'r') as infile:
			for line in infile:
				if self.apikey:
					raise DiceError("Improperly formatted API key file!")
				self.apikey = line

		if not self.apikey:
			raise DiceError("No API key found!")

		self.rand = RandomOrgClient(self.apikey)

	def _print(self, result, before=None):
		"""Printing for calls that are summing in nature."""
		print "The dice roll resulted in: %s" %(result)

		if self.args.verbose == 1:
			if self.args.num:
				addon = self.user_input[-1]
				print "The following numbers were rolled: %s + %s" %(self.alldice, addon)
			else:
				print "The following numbers were rolled: %s" %(self.alldice)

		elif self.args.verbose == 2:
			print "The following numbers were rolled:"
			addon = None
			for key, obj in enumerate(self.user_input):
				if self.args.num and key + 1 == len(self.user_input):
					addon = obj
					continue

				print "\t%s resulted in: %s" %(obj, self.randoms[key])

			if addon:
				print "\t+ %s" %(addon)

	def _print_single(self, result, index=None, objKey=None):
		"""Printing for calls that are min/max in nature."""
		print "The dice roll resulted in: %s" %(result)

		if self.args.verbose == 1:
			print "The following numbers were rolled: %s" %(self.alldice)

		elif self.args.verbose == 2:
			print "The result came from %s on the %s roll." %(self.user_input[objKey], index + 1)
			print "The following numbers were rolled:"
			for key, obj in enumerate(self.user_input):
				print "\t%s resulted in: %s" %(obj, self.randoms[key])

	def _sum(self):
		"""Returns the sum of numbers in alldice."""
		total = 0
		for x in self.alldice:
			total += x

		return total

	def compute(self):
		"""Computes all dice rolls in the user's input."""
		self.randoms = {}
		self.alldice = []

		try:
			for key, obj in self.input.iteritems():
				if not "sides" in obj:
					self.randoms[-1] = obj
					continue

				result = self.rand.generate_integers(obj["num"], 1, obj["sides"])
				self.alldice += result
				self.randoms[key] = result
		except Exception as e:
			"""
			Not sure what all needs to be caught here and the
			documentation on errors thrown by the rdoclient is minimal
			"""
			print "An error occur while retrieving random values!"
			print e
			exit()

	def output(self):
		"""Outputs the programs result to stdout."""
		if self.args.smallest:
			curMinObjKey, curMinIndex, curMinValue = -1, -1, float("inf")
			for key, obj in self.randoms.iteritems():
				minIndex, minValue = min(enumerate(obj), key=lambda p: p[1])

				if minValue < curMinValue:
					curMinObjKey = key
					curMinIndex = minIndex
					curMinValue = minValue

			# sanity check
			if curMinObjKey == -1:
				raise DiceError("Something went horribly wrong!")

			self._print_single(curMinValue, index=curMinIndex, objKey=curMinObjKey)

			return

		elif self.args.largest:
			curMaxObjKey, curMaxIndex, curMaxValue = -1, -1, 0
			for key, obj in self.randoms.iteritems():
				maxIndex, maxValue = max(enumerate(obj), key=lambda p: p[1])

				if maxValue > curMaxValue:
					curMaxObjKey = key
					curMaxIndex = maxIndex
					curMaxValue = maxValue

			# sanity check
			if curMaxObjKey == -1:
				raise DiceError("Something went horribly wrong!")

			self._print_single(curMaxValue, index=curMaxIndex, objKey=curMaxObjKey)

			return

		elif self.args.num:
			before = self._sum()
			total = before + self.randoms[-1]["num"]

			self._print(total, before=before)

			return

		else:
			# case where no modifying command arguments are found
			total = self._sum()

			self._print(total)

			return

class Parser:
	"""Parsing class for dice rolling.

	This class takes input from the user and parses it into data that
	is usable by the dice rolling class.

	"""

	def __init__(self):
		"""Initializing an instance of the Parse class.

		This function creates the required parse object from argparse
		to deal with command line arguments.

		These lines are way past the desired 80th column, but there
		is not much I can do about it :(

		"""
		parser = ArgumentParser(description="Welcome to DiceRoll, a string parser for rolling dice with Random.org!", 
								formatter_class=RawTextHelpFormatter)

		parser.add_argument("-v", "--verbose", action="count", default=0, 
							help="Increase the verbosity of the output\n"
								 "Can be used 1 or 2 times")

		commandGroup = parser.add_mutually_exclusive_group()
		commandGroup.add_argument("-n", "--num", action="store_true", 
								  help="Allows strings of the format <module>+...+<module>+num\n"
									   "num is an integer which will be added to the total of the dice")

		commandGroup.add_argument("-s", "--smallest", action="store_true", 
								  help="returns the smallest number from all the dice rolled")

		commandGroup.add_argument("-l", "--largest", action="store_true", 
								  help="returns the largest number from all the dice rolled")

		parser.add_argument("dice",
							help="string in the form of <module>+<module>+...+<module>\n"
								 "'modules' are in the form <# dice to roll>d<# of sides on the dice>")

		self.args = parser.parse_args()

	def compute(self):
		"""Computes the input for DiceRoll from user input."""
		self.input = {}

		for key, obj in enumerate(self.user_input):
			if self.args.num and (key + 1 == len(self.user_input)):
				self.input[-1] = { "num": int(obj) }
				continue

			tmp = obj.split('d')
			self.input[key] = { "num": int(tmp[0]),
								"sides": int(tmp[1]) }

		return self.input

	def verify(self):
		"""Verifies that the user's input is valid.

		verify() must be called before compute()

		"""
		self.user_input = self.args.dice.split("+")

		if self.args.num:
			if not match('^[0-9]+$', self.user_input[-1]):
				raise DiceError("Improperly formatted input!")

		for index, obj in enumerate(self.user_input):
			# checking for empty strings (ie trailing '+'s)
			if not obj:
				raise DiceError("Improperly formatted input!")

			# checking for valid formatting of each module
			if not match('^[0-9]+[d][0-9]+$', obj):
				""" Checking that lone integers only occur with the num
				flag set and at the end of the input."""
				if self.args.num and match('^[0-9]+$', obj) and (index + 1 == len(self.user_input)):
					continue

				raise DiceError("Improperly formatted input!")

def main():
	"""Main function.

	The main function is what is run when the program is called from
	command line. This function has the responsibility of spawning a 
	parsing class and a dice rolling class. This function will give the
	user data to the parsing class and the output of that to the dice
	rolling class.

	The dice rolling class will return the proper random values for the
	user inputted (is this a word?) dice rolls.

	"""
	parser = Parser()

	try:
		parser.verify()

		diceRoll = DiceRoll(parser.args, parser.compute(), parser.user_input)

		diceRoll.compute()

		diceRoll.output()

	except DiceError as e:
		print "!ERROR!"
		print e

if __name__ == "__main__":
	"""Running the main function."""
	main()